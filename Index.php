<!DOCTYPE html>
<html ng-app="ForzaModule">
<head>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta http-equiv="Content-Type" content="charset=UTF-8">
  <base href="http://localhost/FOURDREAMS/forza_transportes/">
  <title>Meza constructores</title>
  
  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,500|Roboto" rel="stylesheet">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="assets/css/normalize.css">
  <link rel="stylesheet" href="assets/css/skeleton.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="assets/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css">
  <link rel="stylesheet" href="assets/css/custom.css">
  <link rel="stylesheet" href="assets/css/navbarcss.css">
  <!-- javascript -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- slider -->
  <script src="assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <!-- angular -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script> -->
  <script src="assets/js/angular.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-route.js"></script>
  <script  src="assets/js/controlador.js" type="text/javascript"></script>
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon"  href="assets/images/favicon.png">
</head>
<body ng-controller="forzaController">
<!-- Nvbar -->
<section class="background-morado container-navbar">
  <nav  class="topnav" id="myTopnav">
    <a href="#!Home">INICIO</a>
    <a href="#!Nosotros">NOSOTROS</a>
    <a href="#!Servicios">SERVICIOS & SOLUCIONES</a>
    <a href="#!Gcalidad">GESTIÓN DE LA CALIDAD</a>
    <a href="#!Ngente">NUESTRA GENTE</a>
    <a href="#!Contacto">CONTACTO</a>
    <a href="javascript:void(0);" class="icon" onclick="openMenu()">&#9776;</a>
  </nav>  
</section>
<!-- Navabar -->
<div>
    <ng-view></ng-view>
</div>
<footer>
  <div class="container">
    <div class="row">
      <div class="four columns text-center">
        <img class="u-max-full-width footer-image" src="assets/images/footerforza.png">    
      </div>
      <div class="eight columns">
        <div class="row">
          <div class="six columns">
          <label><i class="fa fa-phone"></i> Teléfono de oficinas:</label>
            <span>+52 (55)58725036 / (55)58950126</span>
          </div>
          <div class="six columns">
          <label><i class="fa fa-envelope-o"></i> Correo electrónico:</label>
            <span>contacto@forzatransportes.com.mx</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</body>
</html>
