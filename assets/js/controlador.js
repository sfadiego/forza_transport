var mainApp = angular.module("ForzaModule", ['ngRoute']);
mainApp.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/Home', {
			templateUrl: 'Home.php',
			controller: 'forzaController'
		})
		.when('/Nosotros', {
			templateUrl: 'Nosotros.php',
			controller: 'forzaController'
		})
		.when('/Servicios', {
			templateUrl: 'Servicios.php',
			controller: 'forzaController'
		})
		.when('/Gcalidad', {
			templateUrl: 'Gcalidad.php',
			controller: 'forzaController'
		})
		.when('/Ngente', {
			templateUrl: 'Ngente.php',
			controller: 'forzaController'
		})
		.when('/Contacto', {
			templateUrl: 'Contacto.php',
			controller: 'forzaController'
		})
		.otherwise({
			redirectTo: '/Home'
		});
		// use the HTML5 History API
        //$locationProvider.html5Mode(false);
});

mainApp.controller('forzaController', function($scope) {
	/*Slider */
	$(".regular").slick({
        dots: true
        ,infinite: true
        ,slidesToShow: 1
        ,slidesToScroll: 1
        ,autoplay:true
        ,autoplaySpeed: 3000
        , arrows : false
     });

});


function openMenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
