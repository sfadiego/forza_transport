<div style="position: relative;">
  <section class="regular slider">
    
    <!-- <div style="position: relative;">
      <div class="row">
          <div class="four columns image-slider">
            <img class="u-max-full-width forza-logo-slider1" src="assets/images/logoSlider.png">
          </div>
        </div>
      <img class="u-max-full-width" src="assets/images/slider1.jpg">
    </div> -->
    
  <div style="position: relative;">
        <div class="row">
          <div class="five columns forza-texto-slider">
            <h4 style="color: white;">Tu mejor socio de negocio
                <span class="orange-text">para llevar tu mercancía siempre fría y puntual</span>
            </h4>
          </div>
        </div>
        <img class="u-max-full-width" src="assets/images/slider1.jpg">
  </div>
  </section>
   <div style="position: absolute;bottom: -8px;width: 100%;">
    <div class="shape">
        <div class="top"></div>
        <div class="bottom"></div>
    </div>
   </div>
 </div>
<section class="section-morada">
  <div class="container">
    <div class="row" style="padding-top: 40px; padding-bottom: 40px;">
    <div class="four columns">
      <div class="text-center">
        <img src="assets/images/logo-10-anos.png" class="u-max-full-width img-10-anos" alt="Forza">  
      </div>
      <div class="center-movil">
        <h5 class="orange-text text-right">SIEMPRE FRÍO <br>Y PUNTUAL</h5>
      </div>
    </div>
    <div class="four columns">
      <div class="border-white">
        <div class="row">
          <div class="four columns text-center">
            <img src="assets/images/delivery_white.png" class="u-max-full-width" alt="Forza">  
          </div>
          <div class="eight columns">
            <h6 class="orange-text">
              <b>Nuestro equipo </b><br> de transporte
            </h6>
          </div>
        </div>
        <p>Somos una empresa mexicana con 10 años de experiencia, especializada únicamente en transporte a temperatura controlada con una flota de unidades tipo torton de reciente modelo. </p>
        <a class="button button-gray" href="#">CONOCE NUESTRAS UNIDADES</a>
      </div>
    </div>
    <div class="four columns">
        <div class="row">
          <div class="three columns text-center">
            <img src="assets/images/escudo.png" class="u-max-full-width" alt="Forza">  
          </div>
          <div class="seven columns">
            <h6 class="orange-text">
              <b>Garantía </b><br> de seguridad
            </h6>
          </div>
        </div>
        <p>Garantizamos el transporte y entrega de tu mercancía Siempre Fría y Puntual a cada destino.</p>
        <p>Si prefieres llamárnos, por favor hazlo a los teléfonos:  58725036 / 58950126 </p>
        <a class="button button-gray" href="#">CONOCE MÁS</a>
    </div>
  </div>
  </div>
</section>
<section class="section-gris">
  <div class="container" style="padding-top: 10px;">
    <div class="row">
      <div class="seven columns">
        <h5 class="text-center padding-30-top padding-20-bottom">SOLUCIONES & SERVICIOS</h5>
          <div class="row">
            <div class="six columns">
                <div class="row">
                  <div class="text-center three columns">
                    <img style="" class="u-max-full-width" src="assets/images/light-bulb.png">  
                  </div>
                  <div class="nine columns">
                  <ul class="lista-bullets">
                      <li>Congelado / Refrigerado / Multitemperatura</li>
                      <li>Servicios dedicados </li>
                      <li>Esquemas de renta</li>
                      <li>Entregas a detalle</li>
                      <li>Fletes nacionales / regionales</li>
                      <li>Servicios locales y foráneos con opción a repartos</li>
                      <li>Estadías</li>
                      <li>Maniobras</li>
                      <li>Ruteo</li>
                  </ul>
                  </div>
                </div>
            </div>
            <div class="six columns">
              <div class="row">
                <div class="text-center three columns">
                  <img style="" class="u-max-full-width" src="assets/images/support.png">   
                </div>
                <div class="nine columns">
                  <ul class="lista-bullets">
                    <li>Brindamos atención personalizada a través de nuestra torre de control de monitoreo.</li>
                    <li>Rastreo satelital y de temperatura las 24 horas del día.</li>
                    <li>Renta de unidad para solución de contingencias (almacén)</li>
                    <li>Información de status de tu embarque y flete vía correo electrónico</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="five columns">
        <img class="u-max-full-width width-100p" src="assets/images/solucionServicio.jpg">
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="six columns padding-top-40">
          <img class="u-max-full-width" src="assets/images/Mexico_Map.png">    
      </div>
      <div class="six columns padding-80-top">
        <div class="row">
          <div class="eleven colums">
            <h5>
                <span class="orange-text">Entregamos a</span><b class="morado-text"> NIVEL NACIONAL</b>
            </h5>
          </div>
        </div>
        <div class="row">
            <div class="twelve columns">
              <p>En Forza Transportes nos adaptamos a tus requerimientos logísticos haciendo un traje a la medida para resolver tus necesidades operativas.</p>
              <br>
              <a class="button button-gray" href="#">CONOCE MÁS</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="background-nuestra-gente">
  <div class="container">
    <div class="row padding-80-top">
      <div class="six columns">
          <div class="row">
            <div class="two columns">
                <img style="width: 60px;" class="u-max-full-width" src="assets/images/team.png">    
            </div>
            <div class="ten columns">
              <h6 class="orange-text">Nuestra <br><b>gente</b></h6>
            </div>
          </div>
        <p class="white-text">En FORZA TRANSPORTES nos aseguramos de tener al personal mejor calificado para que la atención y el servicio al cliente sean de primera calidad y así lograr que la mercancía de nuestros clientes siempre llegue fría y puntual a su destino.</p>
        <p class="white-text">Esto podemos lograrlo mediante un riguroso proceso de reclutamiento de todos nuestros operadores y personal administrativo. </p>
        <a class="button button-gray" href="#">CONOCE MÁS</a>
      </div>
      <div class="six columns"></div>
    </div>
  </div>
</section>