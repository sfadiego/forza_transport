<section style="background-color:#00214c; min-height: 250px;">
	<div class="container">
	  	<h6 class="nosotros-titulo" style="padding-top: 100px;">NOSOTROS</h6>
	</div>	
	<div style="width: 100%;">
	    <div class="shape-white">
	        <div class="top-white"></div>
	        <div class="bottom-white"></div>
	    </div>
	</div>
</section>

<section >
 	<div class="container">
	 	<div class="row">
	  		<div class="six columns text-center">
	  			<h6 class="morado-text-bold margin-bottom-10">Mas de  10 años de experiencia</h6>
	  			<h6 class="morado-text">especializados en</h6>
	  		</div>
	  		<div class="six columns text-center">
	  			<h6 class="letter-morado">Nuestros <b>valores</b></h6>
	  		</div>
	  	</div>	
	  	<div class="row">
	  		<div class="six columns">
	  			<div class="row text-center">
	  				<div class="four columns">
	  					<img src="assets/images/delivery.png" class="u-max-full-width" alt="Forza">
	  					<p>
	  						Carga <label>congelada</label>
	  					</p>
	  				</div>
	  				<div class="four columns">
	  					<img src="assets/images/delivery.png" class="u-max-full-width" alt="Forza">
	  					<p>
	  						Carga <label>refrijerada</label>
	  					</p>
	  				</div>
	  				<div class="four columns">
	  					<img src="assets/images/delivery.png" class="u-max-full-width" alt="Forza">
	  					<p>
	  						Multitemperatura
	  					</p>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="six columns">
	  			<div class="row ">
	  				<div class="six columns text-center">
	  					<ul>
	  						<li>Nostrud occaecat.</li>
	  						<li>Nostrud occaecat.</li>
	  						<li>Nostrud occaecat.</li>
	  						<li>Nostrud occaecat.</li>
	  						<li>Nostrud occaecat.</li>
	  					</ul>
	  				</div>
	  				<div class="six columns text-center">
	  					<ul>
	  						<li>Non dolor in.</li>
	  						<li>Non dolor in.</li>
	  						<li>Non dolor in.</li>
	  						<li>Non dolor in.</li>
	  						<li>Non dolor in.</li>
	  					</ul>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
 	</div>
</section>

<div style="width: 100%;">
    <div class="shape-gray">
        <div class="top-gray"></div>
        <div class="bottom-gray"></div>
    </div>
</div>
<section class="background-gray">
	<div class="container">
	  	<div class="row">
	  		<div class="six columns text-center">
	  			<h5 class="letter-morado">Nuestra <b>visión</b></h5>
	  		</div>
	  		<div class="six columns text-center">
	  			<h5 class="letter-morado">Ubicación</h5>
	  		</div>
	  	</div>
	  	<div class="row">
	  		<div class="six columns text-center">
	  			<p>	Labore laborum nostrud duis in qui id occaecat voluptate.
	  				Labore laborum nostrud duis in qui id occaecat voluptate.
	  			</p>
	  		</div>
	  		<div class="six columns">
	  			<p class="letter-blue"><b class="letter-morado">Oficinas centrales</b> Teloyucan, Estado de México</p>
	  			<ul class="lista-bullets">
	  				<li>Cillum ullamco amet sit sunt fugiat eu aliquip ullamco fugiat.</li>
	  				<li>Cillum ullamco amet sit sunt fugiat eu aliquip ullamco fugiat.</li>
	  				<li>Cillum ullamco amet sit sunt fugiat eu aliquip ullamco fugiat.</li>
	  			</ul> 
	  		</div>

	  	</div><br>
	  </div>
<section>
